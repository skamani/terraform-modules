#Module outputs
output "aws_autoscaling_policy_percent_capacity_arn" {
  value = "${aws_autoscaling_policy.percent_capacity.arn}"
}

output "aws_autoscaling_group_name" {
  value = "${aws_autoscaling_group.asg.name}"
}

output "aws_elastic_load_balancer_name" {
  value = "${aws_elb.elb.name}"
}

output "aws_instance_jump_server_id" {
  value = "${aws_instance.jump_server.id}"
}

output "aws_security_group_jump_server_id" {
  value = "${aws_security_group.sg_jump_security.id}"
}

output "elastic_ip_jumpbox" {
  value = "${aws_eip.jumpbox.public_ip}"
}

output "aws_security_group_app_security_id" {
  value = "${aws_security_group.sg_app_security.id}"
}
