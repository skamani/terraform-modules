# Required input vars
variable "env" {}
variable "vpc_id" {}
variable "public_subnet_cidr" {}
variable "gateway_id" {}
variable "nat_id" {}
variable "private_subnet_cidrs" {type = "list"}
