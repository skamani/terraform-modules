# Module outputs
output "aws_sns_topic_hello-world-DEVOPS_arn" {
  value = "${aws_sns_topic.hello-world-DEVOPS-PAGERDUTY.arn}"
}

output "aws_sns_topic_hello-world-DEVOPS-EMAIL_arn" {
  value = "${aws_sns_topic.hello-world-DEVOPS-PAGERDUTY-EMAIL.arn}"
}
